//
//  ViewController.swift
//  Epilities
//
//  Created by Arsh Aulakh on 01/12/2018.
//  Copyright (c) 2018 Arsh Aulakh. All rights reserved.
//

import UIKit
import Epilities

class ViewController: UIViewController {
    
    var picker: PickerView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        picker = PickerView.init(self)
        picker?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func show(_ sender: UIButton) {
        picker?.pickerMode = .time
        picker?.locale = Locale(identifier: "en_GB")
        picker?.show()
    }
    
    @IBAction func hide(_ sender: UIButton) {
        picker?.dismiss()
    }
}

// MARK: - PickerView Delegate Methods
extension ViewController: PickerViewDelegate {
    
    func didSelected(_ picker: UIDatePicker, with date: Date) {
        print(date)
    }
    
    func didCanceled(_ picker: UIDatePicker) {
        print(picker.date)
    }
    
    func didChanged(_ picker: UIDatePicker, with date: Date) {
        print(date)
    }
    
    func failure(_ picker: UIDatePicker, error: Error) {
        print(error.localizedDescription)
    }
}

