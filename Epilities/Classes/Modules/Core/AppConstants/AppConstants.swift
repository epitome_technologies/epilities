//
//  AppConstants.swift
//  Epilities
//
//  Created by Lakhwinder Singh on 24/01/18.
//

import UIKit

public var FONT_BOLD = "HelveticaNeue-Bold"
public var FONT_MEDIUM = "HelveticaNeue-Medium"
public var FONT_REGULAR = "HelveticaNeue"
public var FONT_LIGHT = "HelveticaNeue-Light"
public var NOVAGEMS_BLUE_COLOR = UIColor.init(hexString: "5A4998")
public var NOVAGEMS_DARK_BLUE_COLOR = UIColor.init(hexString: "3C3793")




