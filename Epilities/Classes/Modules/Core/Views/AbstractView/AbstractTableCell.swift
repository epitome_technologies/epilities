//
//  AbstractTableCell.swift
//  FreshMeatMarket
//
//  Created by Lakhwinder Singh on 31/03/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit
import SwipeCellKit

public protocol TableCellDelegate {
    func updateCell()
    func updateModel(_ model: Any, cell: AbstractTableCell)
}

open class AbstractTableCell: SwipeTableViewCell {

    var customDelegate: TableCellDelegate!
    
    open class var reuseId: String {
        return String(describing: self)
    }
    
    open class var aspectRatio: CGFloat {
        get {
            return 0.8;
        }
        set {
            self.aspectRatio = newValue
        }
    }
    
    open class var cellHeight: CGFloat {
        return 44
    }
    
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.perform(#selector(self.initViews), with: self, afterDelay: 0)
    }
    
    @objc open func initViews() {
        self.backgroundColor = UIColor.clear
        clipsToBounds = true
        //Hide Autolayout Warning
        UserDefaults.standard.setValue(false, forKey:"_UIConstraintBasedLayoutLogUnsatisfiable")
    }
    
    open func viewAppear() {
    }
    
    open func viewDisappear() {
    }
    
    open func updateWithModel(_ model: Any) {
    }
    
    open func updateCell() {
        layoutSubviews()
        customDelegate.updateCell()
    }
    
    open func updateModel(_ model: Any) {
        customDelegate.updateModel(model, cell: self)
    }
}
