//
//  AbstractHeaderCell.swift
//  DropDown
//
//  Created by Lakhwinder Singh on 24/01/18.
//

import UIKit

open class AbstractHeaderView: UITableViewHeaderFooterView {
    
    // MARK: Class variables
    public class var reuseId: String {
        return String(describing: self)
    }
    
    // MARK: Initializers
    public override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.initViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initViews()
    }
    
    open func initViews() {
        addSubviews([customBG, titleLabel])
    }
    
    // MARK: Custom heade height
    open class var cellHeight: CGFloat {
        return 30
    }
    
    // MARK: Update model
    open func updateWithModel(_ model: Any) {
        guard let title = model as? String else { return }
        titleLabel.text = title
    }
    
    // MARK: Computed constants
    public let titleLabel: NGUILabel = {
        let label = NGUILabel()
        label.textColor = UIColor.init(hexString: "323A45")
        label.fontStyle = .subheadline
        return label
    }()
    
    open let customBG: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(hexString: "E9EBEC")
        return view
    }()
    
    // MARK: Layout subviews
    override open func layoutSubviews() {
        super.layoutSubviews()
        viewLayout()
        customBG.addConstraintToFillSuperview()
    }
    
    open func viewLayout() {
        let subviews = ["title": titleLabel]
        addVisualConstraints(["H:|-15-[title]-(>=10)-|", "V:|[title]|"], subviews: subviews)
    }
}

