//
//  SegmentButton.swift
//  NovagemsManagerGuards
//
//  Created by Lakhwinder Singh on 19/01/18.
//

import UIKit

var podBundle: Bundle! {
    guard let bundle = Bundle.init(
        podName: "Epilities",
        referenceControllerType: PhotoViewController.self)
    else {
        assertionFailure("Epilities pod bundle is nil")
        return nil
    }
    return bundle
}

open class SegmentButton: UIView {
    
    // MARK: Stores variables
    var viewModel: SegmentButtonViewModel!
    
    // MARK: Class function
    open class func view(viewModel: SegmentButtonViewModel) -> SegmentButton {
        let view = SegmentButton()
        view.viewModel = viewModel
        view.initView()
        return view
    }
    
    // MARK: Initialization
    private func initView() {
        clipsToBounds = false
        addSubviews([titleLabel, borderLine, selectedRowImage])
        backgroundColor = viewModel.backColor
        titleLabel.text = viewModel.text
    }
    
    // MARK: Computed constants
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    private let borderLine: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let selectedRowImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.image(name: "tabbar_selected_arrow", bundle: podBundle)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isHidden = true
        return imageView
    }()
    
    // MARK: Setter getter variables
    open var text: String! {
        get {
            return titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    open var borderLineHidden: Bool {
        get {
            return borderLine.isHidden
        }
        set {
            borderLine.isHidden = newValue
        }
    }
    
    open var selected: Bool {
        get {
            return titleLabel.textColor == viewModel.selectedColor
        }
        set {
            titleLabel.textColor = newValue ? viewModel.selectedColor : viewModel.unSelectedColor
            titleLabel.font = newValue ? viewModel.selectedFont : viewModel.unSelectedFont
            selectedRowImage.isHidden = !newValue
        }
    }
    
    // MARK: Layout subviews
    open override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.addConstraintToFillSuperview()
        
        //Border line constraint
        borderLine.leftAnchor.constraint(equalTo: titleLabel.rightAnchor).isActive = true
        borderLine.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        _ = borderLine.addConstraintForWidth(0.5)
        _ = borderLine.addConstraintForHeight(20.0)
        
        //Selected row constraint
        selectedRowImage.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        selectedRowImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
}
