//
//  SelectionTableCell.swift
//  NovagemsManagerSites
//
//  Created by Lakhwinder Singh on 31/01/18.
//

import UIKit

open class AbstractSelectionTableCell: AbstractTableCell {
    
    // MARK: Stored variables
    public var leftWConstraint: NSLayoutConstraint!
    
    // MARK: Can be override variables
    open var selBGColor: UIColor {
        return UIColor.init(hexString: "FFEFF3")//Light red colour
    }
    
    open var selImage: UIImage {
        return UIImage.image(name: "img_multi_select_selected", bundle: epilitiesPodBundle)
    }
    
    open var unSelImage: UIImage {
        return UIImage.image(name: "img_multi_select_normal", bundle: epilitiesPodBundle)
    }
    
    open override var editingEnabled: Bool {
        return true
    }
    
    // MARK: Initializers
    override open func initViews() {
        super.initViews()
        addSubviews([leftContainer, mainContainer])
        leftContainer.addSubview(selectionImageView)
        selectionImageView.image = unSelImage
    }
    
    // MARK: Manage editing functions
    override open func setEditing(_ editing: Bool, animated: Bool) {
        self.leftWConstraint.constant = editing ? 60 : 0
        self.layoutIfNeeded()
    }
    
    open override func editSelectedCell(_ selected: Bool) {
        selectionImageView.image = selected ? selImage : unSelImage
        backgroundColor = selected ? selBGColor : .clear
    }
    
    // MARK: Computed constants
    public let leftContainer: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
    
    public let selectionImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    public let mainContainer: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
    
    // MARK: Layout subviews
    override open func layoutSubviews() {
        super.layoutSubviews()
        if leftWConstraint == nil {
            leftWConstraint = leftContainer.addConstraintForWidth(0)
        }
        _ = selectionImageView.addConstraintForWidth(40)
        _ = selectionImageView.addConstraintForHeight(40)
        addConstraintSameCenterXY(leftContainer, and: selectionImageView)
        addVisualConstraints(["H:|[left][main]|", "V:|[left]|", "V:|[main]|"], subviews: ["left": leftContainer, "main": mainContainer])
    }
    
}
