//
//  SegmentButtonViewModel.swift
//  NovagemsManagerGuards
//
//  Created by Lakhwinder Singh on 19/01/18.
//

import UIKit

public struct SegmentButtonViewModel {
    var text: String
    var ratio: CGFloat
    var backColor: UIColor
    var selectedFont: UIFont
    var unSelectedFont: UIFont
    var selectedColor: UIColor
    var unSelectedColor: UIColor
    
    public init(
        text: String,
        ratio: CGFloat = 0.0,
        backColor: UIColor = UIColor.init(red: 0.1882, green: 0.2157, blue: 0.2588, alpha: 1.0),
        selectedFont: UIFont = UIFont.boldSystemFont(ofSize: 17.0),
        unSelectedFont: UIFont = UIFont.systemFont(ofSize: 17.0),
        selectedColor: UIColor = UIColor.white,
        unSelectedColor: UIColor = UIColor.lightGray
    ) {
        self.text = text
        self.ratio = ratio
        self.backColor = backColor
        self.selectedFont = selectedFont
        self.unSelectedFont = unSelectedFont
        self.selectedColor = selectedColor
        self.unSelectedColor = unSelectedColor
    }
}


