//
//  NovagemsSegmentView.swift
//  NovagemsManagerGuards
//
//  Created by Lakhwinder Singh on 19/01/18.
//

import UIKit

/*
 This segment view with multiple option designed for novagems application.
 */
open class NovagemsSegmentView: UIView {
    
    // MARK: Constant variables
    let viewHeight: CGFloat = 50
    
    // MARK: Stored variables
    var segmentButtons: [SegmentButton] = []
    var viewModels: [SegmentButtonViewModel] = []
    var closure: ((_: Int)->Void)!
    var isXibUsed = false
    var currentSelectedIndex = 0

    // MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isXibUsed = true
        initView()
    }
    
    func initView() {
        clipsToBounds = false
    }
    
    // MARK: Update segments
    open func updateSegments(_ viewModels: [SegmentButtonViewModel], _ completionBlock: ((_: Int)->Void)?) {
        self.viewModels = viewModels
        self.closure = completionBlock
        //Remove existing subviews
        for view in segmentButtons {
            view.removeFromSuperview()
        }
        segmentButtons.removeAll()
        //Create new array with updated view models
        for i in 0..<viewModels.count {
            let viewModel = viewModels[i]
            backgroundColor = viewModel.backColor //Background color should be same as segment backgrounds.
            let segment = SegmentButton.view(viewModel: viewModel)
            segment.borderLineHidden = i == viewModels.count-1 //Hide border line for last segment
            segment.actionBlock {
                self.currentSelectedIndex = i
                self.selectedIndex(self.currentSelectedIndex)
            }
            segmentButtons.append(segment)
        }
        addSubviews(segmentButtons)
        selectedIndex(currentSelectedIndex)
        layoutIfNeeded()
    }
    
    private func selectedIndex(_ index: Int) {
        for i in 0..<segmentButtons.count {
            let segment = segmentButtons[i]
            segment.selected = i == index
        }
        
        guard self.closure != nil else {
            return
        }
        self.closure!(index)
    }
    
    // MARK: Layout subivews
    open override func layoutSubviews() {
        super.layoutSubviews()
        if !isXibUsed {
            _ = addConstraintForHeight(viewHeight)
        }
        guard segmentButtons.count != 0 else {
            return
        }
        let firstItem = segmentButtons.first
        let lastItem = segmentButtons.last
        addVisualConstraints(["H:|[first]", "H:[last]|"], subviews: ["first": firstItem!, "last": lastItem!])
        
        for i in 0..<segmentButtons.count {
            let segmentButton = segmentButtons[i]
            addVisualConstraints(["V:|[current]|"], subviews: ["current": segmentButton])
            
            let ratio = viewModels[i].ratio
            if ratio != 0 {
                _ = segmentButton.addConstraintForWidth(width * ratio)
            }
            if segmentButton != lastItem {
                let nextSegment = segmentButtons[i+1]
                addVisualConstraints(["H:[current]-5-[next]"], subviews: ["current": segmentButton, "next": nextSegment])
                if ratio == 0 {
                    segmentButton.addConstraintSameWidth(nextSegment)
                }
            }
        }
    }
    
}


