//
//  NGLabel.swift
//  Epilities
//
//  Created by Lakhwinder Singh on 09/02/18.
//

import UIKit

public class NGUILabel: UILabel {

    // MARK: Initializers
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initValues()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        initValues()
    }
    
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        initValues()
    }

    public func initValues() {
        if #available(iOS 10.0, *) {
            adjustsFontForContentSizeCategory = true
        }
        adjustsFontSizeToFitWidth = true
        font = UIFont.preferredFont(forTextStyle: fontStyle)
    }
    
    // MARK: Didset variables
    public var fontStyle: UIFontTextStyle = .caption1 {
        didSet {
            font = UIFont.preferredFont(forTextStyle: fontStyle)
        }
    }
}
