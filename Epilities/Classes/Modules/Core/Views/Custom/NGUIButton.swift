//
//  NGUIButton.swift
//  Epilities
//
//  Created by Lakhwinder Singh on 09/02/18.
//

import UIKit

open class NGUIButton: UIButton {

    // MARK: Initializers
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initValues()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        initValues()
    }
    
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        initValues()
    }
    
    open func initValues() {
        if #available(iOS 10.0, *) {
            titleLabel?.adjustsFontForContentSizeCategory = true
        }
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.font = UIFont.preferredFont(forTextStyle: fontStyle)
    }
    
    // MARK: Didset variables
    public var fontStyle: UIFontTextStyle = .caption1 {
        didSet {
            titleLabel?.font = UIFont.preferredFont(forTextStyle: fontStyle)
        }
    }
}
