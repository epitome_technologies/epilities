//
//  AbstractTableListControl.swift
//  FreshMeatMarket
//
//  Created by Lakhwinder Singh on 31/03/17.
//  Copyright © 2017 lakh. All rights reserved.
//

import UIKit
import SwipeCellKit

/*
 Do not use AbstractTableListControl direct as parent control. It needs to managed from another abstract controls.
 */
open class AbstractTableListControl: UITableViewController, TableCellDelegate, TableListUtilities, SwipeTableViewCellDelegate {
    
    // MARK: Initializers
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public required override init(style: UITableViewStyle) {
        super.init(style: style)
    }
    
    // MARK: Must override variable
    
    open var cellClass: AbstractTableCell.Type {
        return AbstractTableCell.self
    }
    
    // MARK: Variables can be override
    
    open var customNoItemText: String {
        return "No data available!"
    }
    
    open var isNibUsed: Bool {
        return false
    }
    
    open var isStaticData: Bool {
        return false
    }
 
    // MARK: Custom enums
    
    public enum RequestPageType : Int {
        case first = 0
        case next = 1
    }
    
    // MARK: Stored variables
    
    open var mainControl: UIViewController!
    open var isLoading = false
    open var isPageAvailable : Bool!
    open var isConnection = true
    open var isRefreshing = false
    open var offset : NSInteger = 0
    private var _searchQuery: String!
    
    // MARK: Setter getter variables
    
    open var searchQuery: String! {
        get {
            return _searchQuery ?? ""
        }
        set {
            if newValue != _searchQuery {
                _searchQuery = newValue
                requestItemsForFirstPage()
            }
        }
    }
    
    open var noItemsText: String {
        get {
            return self.customBackgroundView.messageText
        }
        set {
            if isConnection || newValue.count != 0 {
                self.customBackgroundView.messageText = newValue
            }
            if !isConnection {
                self.customBackgroundView.messageText = noInternetText
            }
        }
    }
    
    let customBackgroundView: ListBackgroundView = {
        let background = ListBackgroundView()
        background.isHidden = true
        return background
    }()
    
    // MARK: View load functions
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.requestItemsForFirstPage()
        // Do any additional setup after loading the view.
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        for cell in self.tableView.visibleCells {
            (cell as! AbstractTableCell).viewDisappear()
        }
    }
    
    // MARK: init view
    
    func setupTableView() {
        // Register cell classes
        view.backgroundColor = .white
        tableView.estimatedRowHeight = cellClass.cellHeight
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .singleLine
        tableView.clipsToBounds = true
        tableView.separatorInset = .zero
        tableView.tableFooterView = UIView()
        if isNibUsed {
            tableView!.register((UINib.init(nibName: String(describing: cellClass), bundle: nil)), forCellReuseIdentifier: reuseId)
        } else {
            tableView!.register(self.cellClass, forCellReuseIdentifier:self.reuseId)
        }
        tableView!.showsVerticalScrollIndicator = false;
        tableView!.alwaysBounceVertical = true;
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(AbstractTableListControl.refresh), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        tableView?.contentInset = .init(top: 0, left: 0, bottom: 8, right: 0)
        tableView?.backgroundView = self.customBackgroundView
    }
    
    func addContentOffsetObserver() {
        self.tableView!.addObserver(self, forKeyPath: "contentOffset", options: NSKeyValueObservingOptions.new, context: nil);
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "contentOffset") {
            
        }
    }
    
    // MARK: requests
    
    func requestItemsForFirstPage() {
        if isLoading == false {
            isLoading = true
            self.isPageAvailable = true
            offset = 0
            self.requestItemsForPage(.first)
        }
    }
    
    func requestItemsForNextPage() {
        if isLoading == false && self.isPageAvailable == true {
            isLoading = true
            offset = offset+itemsPerPage
            self.requestItemsForPage(.next)
        }
    }

    
    func reloadValues() {
        tableView!.reloadData()
        customBackgroundView.searchText = nil
        isLoading = false
        endRefreshing()
    }
    
    open func endRefreshing() {
        self.refreshControl?.endRefreshing()
    }
    
    // MARK: Actions
    
    @objc open func refresh() {
        isRefreshing = true
        self.requestItemsForFirstPage()
    }
    
    // MARK: Must override functions
    
    open func requestItemsForPage(_ type: RequestPageType) {}
    
    open func cellConfiguration(_ cell: AbstractTableCell, _ indexPath: IndexPath) {}
    
    // MARK: UITableViewDataSource and Delegates
    
    open func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return nil
    }
    
    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: AbstractTableCell!
        if isNibUsed {
            cell = tableView.dequeueReusableCell(withIdentifier: self.reuseId) as! AbstractTableCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: self.reuseId, for: indexPath) as! AbstractTableCell
        }
        
        cell.customDelegate = self
        cell.delegate = self
        cell.selectionStyle = .none
        cell.accessoryType = .none
        cellConfiguration(cell, indexPath)
        return cell
    }
    
    // MARK: Cell delegate methods
    
    open func updateCell() {
        tableView.reloadData()
    }
    
    open func updateModel(_ model: Any, cell: AbstractTableCell) {}
    
    open override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! AbstractTableCell).viewDisappear()
    }
    
    // MARK: Misc
    
    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
