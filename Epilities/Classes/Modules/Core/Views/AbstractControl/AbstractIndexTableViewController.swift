//
//  AbstractIndexTableViewController.swift
//  Floaty
//
//  Created by Lakhwinder Singh on 22/01/18.
//

import UIKit

/*
 AbstractIndexTableViewController can be used as parent control. We need to pass data in dictionary(header text and array) and cell class to be define.
 */
open class AbstractIndexTableViewController: AbstractTableListControl {
    
    // MARK: Stored variables
    open var items: [String: Array<Any>] = [:]

    // MARK: Setter getter variables
    open var staticItems: [String: Array<Any>] {
        get {
            return items
        }
        set {
            items.removeAll()
            items = newValue
            reloadValues()
        }
    }

    // MARK: requests
    override open func requestItemsForPage(_ type: RequestPageType) {
        if (self.searchQuery.count != 0) {
            customBackgroundView.isHidden = false
            customBackgroundView.searchText = searchQuery
            self.items.removeAll()
            self.tableView!.reloadData()
        } else {
            self.customBackgroundView.isHidden = true
        }

        //MARK: Make a request
        if isConnection || isStaticData {
            noItemsText = customNoItemText
            self.requestItems(self.searchQuery, limit: itemsPerPage, offset: offset, completion:{(items, error, pageAvailable) in
                self.isPageAvailable = pageAvailable
                self.items.removeAll()
                if (error == nil && items != nil) {
                    self.items = items!
                }
                self.reloadValues()
            })
        } else {
            noItemsText = ""
            items.removeAll()
            isPageAvailable = false
            reloadValues()
        }
    }

    open override func reloadValues() {
        super.reloadValues()
        self.customBackgroundView.isHidden = (self.items.count != 0)
    }

    open func requestItems(_ query: String, limit: NSInteger, offset: NSInteger, completion: @escaping (_ : [String: Array<Any>]?, _ : Error?, _ : Bool?) -> Void) {
        completion(nil, nil, false); // Default implementation does almost nothing
    }

    open override func cellConfiguration(_ cell: AbstractTableCell, _ indexPath: IndexPath) {
        cell.updateWithModel(sectionArray(indexPath.section)![indexPath.row])
    }

    // MARK: UITableViewDataSource and delegates
    
    open override func numberOfSections(in tableView: UITableView) -> Int {
        return items.keys.count
    }

    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (sectionArray(section)?.count)!
    }

    open override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! AbstractTableCell).viewAppear()

        let shouldPreloadHere = indexPath.item == self.items.count - itemsPerPage + preloadAtItemTable;
        let lastItemReached = indexPath.item == self.items.count - 1;
        if (shouldPreloadHere || lastItemReached) {
            self.requestItemsForNextPage()
        }
    }

    open override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = (items.keys.sorted{$1>$0})[section]
        return title
    }
    
    // MARK: Getters variables and functions
    
    private func sectionArray(_ section: Int) -> Array<Any>? {
        return items[Array(items.keys.sorted{$1>$0})[section]]
    }

    // MARK: Cell delegate methods
    
    override open func updateModel(_ model: Any, cell: AbstractTableCell) {
        guard let indexPath = tableView.indexPathForRow(at: cell.frame.origin) else { return }
        var arrayData = sectionArray(indexPath.section)
        guard arrayData != nil else { return }
        arrayData![indexPath.row] = model
        items[Array(items.keys.sorted{$1>$0})[indexPath.section]] = arrayData
    }
}
