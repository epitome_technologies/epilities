//
//  AbstractTableViewController.swift
//  Floaty
//
//  Created by Lakhwinder Singh on 22/01/18.
//

import UIKit

/*
 AbstractTableViewController can be used as parent control. We need to pass data in array and cell class to be define.
 */
open class AbstractTableViewController: AbstractTableListControl {

    // MARK: Stored variables
    open var items: Array<Any> = []

    // MARK: Setter getter variables
    open var staticItems: Array<Any> {
        get {
            return items
        }
        set {
            items.removeAll()
            items.append(contentsOf: newValue)
            reloadValues()
        }
    }
    
    // MARK: Override required functions
    override open func requestItemsForPage(_ type: RequestPageType) {
        if (self.searchQuery.count != 0) {
            customBackgroundView.isHidden = false
            customBackgroundView.searchText = searchQuery
            self.items.removeAll()
            self.tableView!.reloadData()
        } else {
            self.customBackgroundView.isHidden = true
        }
        
        if isConnection || isStaticData {
            noItemsText = customNoItemText
            self.requestItems(self.searchQuery, limit: itemsPerPage, offset: offset, completion:{(items, error, pageAvailable) in
                self.isPageAvailable = pageAvailable
                if (error == nil && items != nil) {
                    if (type == .first) {
                        self.items.removeAll()
                    }
                    self.items.append(contentsOf: items!)
                } else {
                    if (type == .first && error != nil) {
                        self.items.removeAll()
                    }
                }
                self.reloadValues()
            })
        } else {
            noItemsText = ""
            items.removeAll()
            isPageAvailable = false
            reloadValues()
        }
    }
    
    override func reloadValues() {
        super.reloadValues()
        self.customBackgroundView.isHidden = (self.items.count != 0)
    }
    
    open func requestItems(_ query: String, limit: NSInteger, offset: NSInteger, completion: @escaping (_ : Array<Any>?, _ : Error?, _ : Bool?) -> Void) {
        completion(nil, nil, false); // Default implementation does almost nothing
    }
    
    override open func cellConfiguration(_ cell: AbstractTableCell, _ indexPath: IndexPath) {
        cell.updateWithModel(items[indexPath.row])
    }
    
    // MARK: UITableViewDataSource and Delegates
    open override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    open override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! AbstractTableCell).viewAppear()
        
        let shouldPreloadHere = indexPath.item == self.items.count - itemsPerPage + preloadAtItemTable
        let lastItemReached = indexPath.item == self.items.count - 1
        if (shouldPreloadHere || lastItemReached) {
            requestItemsForNextPage()
        }
    }
    
    // MARK: Cell delegate methods
    override open func updateModel(_ model: Any, cell: AbstractTableCell) {
        let indexPath = tableView.indexPathForRow(at: cell.frame.origin)
        let row: Int = (indexPath?.row)!
        self.items[row] = model
    }
}
