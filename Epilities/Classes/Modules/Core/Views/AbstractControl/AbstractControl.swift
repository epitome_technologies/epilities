//
//  AbstractControl.swift
//  Whodunnit
//
//  Created by Lakhwinder Singh on 03/09/17.
//  Copyright © 2017 LakhwinderSingh. All rights reserved.
//

import UIKit

/*
 Use AbstractControl to manage common functions in this class.
 */
open class AbstractControl: UIViewController {

    // MARK:- View did load function
    override open func viewDidLoad() {
        super.viewDidLoad()
        initListControl()
    }
    
    open func initListControl() {
        if listContainer != nil {
            if tableClass != nil {
                customAddChildViewController(tableControl(tableClass), subview: listContainer)
            }
        }
    }
    
    // MARK:- Setter getter variable
    @IBOutlet open var listContainer: UIView!
    
    open var tableClass: AbstractTableListControl.Type! {
        return nil
    }
    
    open func tableControl(_ tableClass: AbstractTableListControl.Type!) -> AbstractTableListControl {
        let tableControl = tableClass.init(style: .plain)
        tableControl.mainControl = self
        return tableControl
    }
    
    // MARK:- Misc
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
