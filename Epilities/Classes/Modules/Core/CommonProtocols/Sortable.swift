//
//  Sortable.swift
//  NovagemsManagerGuards
//
//  Created by Lakhwinder Singh on 23/01/18.
//

import UIKit

public protocol Sortable {
    var name: String { get set }
}

public extension Array where Element: Sortable {
    
    public func filteredNames() -> [String : Array<Element>] {
        let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".map({ String($0) })
        var result: [String: Array<Element>] = [:]
        for letter in alphabet {
            let matches = self.filter({ $0.name.capitalized.hasPrefix(letter) })
            if !(matches.isEmpty) {
                let newLetter = "    "+letter  // Add space to letter from left.
                result[newLetter] = []
                for word in matches {
                    result[newLetter]?.append(word)
                }
            }
        }
        return result
    }
}


