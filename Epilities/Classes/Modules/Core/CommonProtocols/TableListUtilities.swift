//
//  TableListUtilities.swift
//  Floaty
//
//  Created by Lakhwinder Singh on 22/01/18.
//

import UIKit

public protocol TableListUtilities {
    
    // MARK: Variables with getter access only
    var cellClass: AbstractTableCell.Type { get }
    var itemsPerPage: Int { get }
    var preloadAtItemTable: Int { get }
    var noInternetText: String { get }
    var customNoItemText: String { get }
    var reuseId: String { get }
    var isNibUsed: Bool { get}
    var isStaticData: Bool { get}
    
    // MARK: Variables with getter setter access
    var searchQuery: String! { get set }
    var noItemsText: String { get set }
    
}

public extension TableListUtilities {
    
    var itemsPerPage: Int {
        return 20
    }
    
    var preloadAtItemTable: Int {
        return 8
    }
    
    var noInternetText: String {
        return "No internet connection!"
    }
    
    var reuseId: String {
        return cellClass.reuseId
    }
}
