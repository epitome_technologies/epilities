//
//  UIView.swift
//  NovagemsManagerGuards
//
//  Created by Lakhwinder Singh on 19/01/18.
//

import UIKit

public extension UIView {
    
    public var height: CGFloat {
        get {
            return bounds.size.height
        }
        set {
            frame.size.height = newValue
        }
    }
    
    public var width: CGFloat {
        get {
            return bounds.size.width
        }
        set {
            frame.size.width = newValue
        }
    }
    
    public func actionBlock(_ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        
        let recognizer = UITapGestureRecognizer(target: sleeve, action: #selector(ClosureSleeve.invoke(_:)))
        addGestureRecognizer(recognizer)
        isUserInteractionEnabled = true
        
        objc_setAssociatedObject(self, String(format: "[%d]", arc4random()), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
    
    public func bringSubviewToFront(_ subview: UIView, withSuperviews number: Int) {
        var subview = subview
        for _ in 0...number {
            subview.superview?.bringSubview(toFront: subview)
            subview = subview.superview!
        }
    }
    
    public func addConstraint(_ view1: UIView, view2: UIView, relatedBy: NSLayoutRelation = .equal, att1: NSLayoutAttribute, att2: NSLayoutAttribute, mul: CGFloat, const: CGFloat) -> NSLayoutConstraint {
        if view2.responds(to: #selector(setter: self.translatesAutoresizingMaskIntoConstraints)) {
            view2.translatesAutoresizingMaskIntoConstraints = false
        }
        let constraint = NSLayoutConstraint(item: view1, attribute: att1, relatedBy: relatedBy, toItem: view2, attribute: att2, multiplier: mul, constant: const)
        addConstraint(constraint)
        return constraint
    }
    
    public func addConstraint(_ view: UIView, att1: NSLayoutAttribute, att2: NSLayoutAttribute, mul: CGFloat, const: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: att1, relatedBy: .equal, toItem: view, attribute: att2, multiplier: mul, constant: const)
        addConstraint(constraint)
        return constraint
    }
    
    public func addConstraintSameCenterX(_ view1: UIView, view2: UIView) {
        _ = addConstraint(view1, view2: view2, att1: .centerX, att2: .centerX, mul: 1.0, const: 0.0)
    }
    
    public func addConstraintSameCenterY(_ view1: UIView, view2: UIView) {
        _ = addConstraint(view1, view2: view2, att1: .centerY, att2: .centerY, mul: 1.0, const: 0.0)
    }
    
    public func addConstraintSameHeight(_ view2: UIView) {
        _ = superview?.addConstraint(self, view2: view2, att1: .height, att2: .height, mul: 1.0, const: 0.0)
    }
    
    public func addConstraintSameWidth(_ view2: UIView) {
        _ = superview?.addConstraint(self, view2: view2, att1: .width, att2: .width, mul: 1.0, const: 0.0)
    }
    
    public func addConstraintSameCenterXY(_ view1: UIView, and view2: UIView) {
        _ = addConstraintSameCenterX(view1, view2: view2)
        _ = addConstraintSameCenterY(view1, view2: view2)
    }
    
    public func addConstraintSameSize(_ view2: UIView) {
        _ = addConstraintSameWidth(view2)
        _ = addConstraintSameHeight(view2)
    }
    
    public func addConstraintSameAttribute(_ attribute: NSLayoutAttribute, subviews: [UIView]) {
        for i in 1..<subviews.count {
            addConstraint(NSLayoutConstraint(item: subviews[0], attribute: attribute, relatedBy: .equal, toItem: subviews[i], attribute: attribute, multiplier: 1.0, constant: 0.0))
        }
    }
    
    public func addVisualConstraints(_ constraints: [String], subviews: [String: UIView]) {
        addVisualConstraints(constraints, metrics: nil, forSubviews: subviews)
    }
    
    public func addVisualConstraints(_ constraints: [String], metrics: [String: Any]?, forSubviews subviews: [String: UIView]) {
        // Disable autoresizing masks translation for all subviews
        for subview in subviews.values {
            if subview.responds(to: #selector(setter: self.translatesAutoresizingMaskIntoConstraints)) {
                subview.translatesAutoresizingMaskIntoConstraints = false
            }
        }
        // Apply all constraints
        for constraint in constraints {
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: constraint, options: [], metrics: metrics, views: subviews))
        }
    }
    
    public func addConstraintToFillSuperview() {
        superview?.addVisualConstraints(["H:|[self]|", "V:|[self]|"], subviews: ["self": self])
    }
    
    public func removeConstraints() {
        var superview: UIView? = self.superview
        while superview != nil {
            for c: NSLayoutConstraint in (superview?.constraints)! {
                if c.firstItem as! NSObject == self || (c.secondItem != nil && c.secondItem as! NSObject == self) {
                    superview?.removeConstraint(c)
                }
            }
            superview = superview?.superview
        }
    }
    
    public func addConstraintForAspectRatio(_ aspectRatio: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: self, attribute: .height, multiplier: aspectRatio, constant: 0.0)
        addConstraint(constraint)
        return constraint
    }
    
    public func addConstraintForWidth(_ width: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0.0, constant: width)
        addConstraint(constraint)
        return constraint
    }
    
    public func addConstraintForHeight(_ height: CGFloat, relatedBy: NSLayoutRelation = .equal) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: relatedBy, toItem: nil, attribute: .notAnAttribute, multiplier: 0.0, constant: height)
        addConstraint(constraint)
        return constraint
    }
    
    public func addSubviews(_ subviews: [UIView]) {
        for view in subviews {
            addSubview(view)
        }
    }
}

class ClosureSleeve {
    let closure: ()->()
    
    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }
    
    @objc func invoke(_ sender: UITapGestureRecognizer) {
        guard let view = sender.view else {
            assertionFailure("Not reachable")
            return
        }
        view.isUserInteractionEnabled = false
        closure()
        view.isUserInteractionEnabled = true
    }
    
}


