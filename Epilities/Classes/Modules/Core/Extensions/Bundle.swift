//
//  Bundle.swift
//  LoginController
//
//  Created by ET on 2017-12-27.
//

import Foundation

internal extension Bundle {
    
    /**
     Initialise pod bundle
     */
    internal convenience init?(
        podName: String,
        referenceControllerType: UIViewController.Type) {
        let podBundle = Bundle.init(for: referenceControllerType)
        guard let bundleUrl = podBundle.url(forResource: podName, withExtension: "bundle") else {
            assertionFailure("Bundle was nil for \(referenceControllerType)")
            return nil
        }
        self.init(url: bundleUrl)
    }
}
