//
//  UIApplication.swift
//  Reminisce
//
//  Created by Lakhwinder Singh on 23/02/17.
//  Copyright © 2017 paige. All rights reserved.
//

import UIKit

public extension UIApplication {
    
    public class var appWindow: UIWindow! {
        return (UIApplication.shared.delegate?.window!)!
    }
    
    public class var rootViewController: UIViewController! {
        return self.appWindow.rootViewController!
    }
    
    public class var visibleViewController: UIViewController! {
        return self.rootViewController.findContentViewControllerRecursively()
    }
    
    public class var visibleNavigationController: UINavigationController! {
        return self.visibleViewController.navigationController
    }
    
    public class var visibleTabBarController: UITabBarController! {
        return self.visibleViewController.tabBarController!
    }
    
    public class var visibleSplitViewController: UISplitViewController! {
        return self.visibleViewController.splitViewController!
    }

    public class func pushOrPresent(_ viewController: UIViewController, animated: Bool) {
        if self.visibleNavigationController != nil {
            self.visibleNavigationController.pushViewController(viewController, animated: animated)
        } else {
            self.visibleViewController.present(viewController, animated: animated, completion: nil)
        }
    }
    
    public class var appWindowFrame: CGRect {
        return self.appWindow.frame
    }
    
    public class var navigationBarFrame: CGRect {
        return self.visibleNavigationController.navigationBar.frame
    }
    
    public class var navigationBarHeight: CGFloat {
        return self.navigationBarFrame.size.height
    }
    
    public class var statusBarHeight: CGFloat {
        return self.shared.statusBarFrame.size.height
    }
    
    public class var tabBarFrame: CGRect {
        return self.visibleTabBarController.tabBar.frame
    }
    
    public class var tabBarHeight: CGFloat {
        return self.tabBarFrame.size.height
    }

    public class var interfaceOrientation: UIInterfaceOrientation {
        return self.shared.statusBarOrientation
    }
    
    public class var interfaceOrientationIsLandscape: Bool {
        return self.interfaceOrientation == .landscapeLeft || self.interfaceOrientation == .landscapeRight
    }
    
    public class var interfaceOrientationIsPortrait: Bool {
        return self.interfaceOrientation == .portrait
    }
    
    public class var interfaceOrientationIsPortraitOrUpsideDown: Bool {
        return self.interfaceOrientation == .portrait || self.interfaceOrientation == .portraitUpsideDown
    }
}


