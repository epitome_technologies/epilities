//
//  UIImage.swift
//  Floaty
//
//  Created by Lakhwinder Singh on 19/01/18.
//

import UIKit

public extension UIImage {
    
    public class func image(name: String, bundle: Bundle) -> UIImage! {
        return UIImage(named: name, in: bundle, compatibleWith: UITraitCollection.init())
    }

}


