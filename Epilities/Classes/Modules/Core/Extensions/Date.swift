//
//  Date.swift
//  NovagemsManagerSites
//
//  Created by Lakhwinder Singh on 08/02/18.
//

import UIKit

public extension Date {

    public func string(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    public func addDays(_ days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    public var startDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    public var endDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        guard let newDate = Calendar.current.date(byAdding: components, to: startDay)
            else { return startDay.addDays(1) }
        return newDate
    }
    
    public mutating func mergeDate(_ date2: Date,
                          components: [Calendar.Component] = [.hour, .minute])
    {
        for component in components {
            let dateComponenet = Calendar.current.dateComponents([component], from: date2)

            self = Calendar.current.date(bySetting: component, value: dateComponenet.getValue(component), of: self) ?? self
        }
    }
    
    /// Returns the amount of days from another date
    public func difference(_ date: Date) -> DateComponents {
        return Calendar.current.dateComponents([.day, .hour, .minute, .second], from: date, to: self)
    }
    
    public func daysDifference(_ date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date.startDay, to: self.startDay).day ?? 0
    }
}

extension DateComponents {
    
    public func getValue(_ calComponenet: Calendar.Component) -> Int {
        var value = 0
        switch calComponenet {
        case .day:
            value = self.day ?? 0
        case .month:
            value = self.month ?? 0
        case .year:
            value = self.year ?? 0
        case .weekday:
            value = self.weekday ?? 0
        case .weekOfYear:
            value = self.weekOfYear ?? 0
        case .hour:
            value = self.hour ?? 0
        case .minute:
            value = self.minute ?? 0
        case .second:
            value = self.second ?? 0
        default:
            assertionFailure("Check pending componenet")
        }
        return value
    }
}

extension DateFormatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        /// formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        return formatter
    }()
}
