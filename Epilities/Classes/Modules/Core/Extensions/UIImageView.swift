//
//  UIImageView.swift
//  NovagemsManagerSites
//
//  Created by Lakhwinder Singh on 07/02/18.
//

import UIKit

public extension UIImageView {

    public func openImage() {
        guard let control = UIApplication.visibleViewController else { return }
        control.present(self.imageViewer(), animated: true, completion: nil)
    }
    
    public func imageViewer(buttons: [UIPreviewAction] = []) -> ImageViewerController {
        let configuration = ImageViewerConfiguration { config in
            config.imageView = self
            config.buttons = buttons
        }
        return ImageViewerController(configuration: configuration)
    }
}
