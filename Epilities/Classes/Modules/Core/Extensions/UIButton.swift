//
//  UIButton.swift
//  Mindfulness Master
//
//  Created by Lakh on 02/07/17.
//  Copyright © 2017 Lakh. All rights reserved.
//

import UIKit

public extension UIButton {

    public func buttonClick(_ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke(_:)), for: .touchUpInside)
        objc_setAssociatedObject(self, String(format: "[%d]", arc4random()), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
    
    public var image: UIImage {
        get {
            return image(for: .normal)!
        }
        set {
            setImage(newValue, for: .normal)
        }
    }
    
    public var title: String {
        get {
            return title(for: .normal)!
        }
        set {
            setTitle(newValue, for: .normal)
        }
    }
    
    public var titleColor: UIColor {
        get {
            return titleColor(for: .normal)!
        }
        set {
            setTitleColor(newValue, for: .normal)
        }
    }

}


