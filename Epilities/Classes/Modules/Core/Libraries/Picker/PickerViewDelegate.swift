//
//  PickerViewDelegate.swift
//  Epilities
//
//  Created by Epitome Technologies on 13/01/18.
//

import UIKit

// UIDatePicker View Delegate Methods
@objc public protocol PickerViewDelegate: NSObjectProtocol {
    
    func didSelected(_ picker: UIDatePicker, with date: Date)
    func didCanceled(_ picker: UIDatePicker)
    func failure(_ picker: UIDatePicker, error: Error)
    
    @objc optional func didChanged(_ picker: UIDatePicker, with date: Date)
}
