//
//  PickerView.swift
//  NovagemsManager
//
//  Created by Epitome Technologies on 19/12/17.
//  Copyright © 2017 Epitome Technologies. All rights reserved.
//

import UIKit

private enum Origin {
    static let x: CGFloat = 8
    static let y: CGFloat = 60
}

private enum Size {
    static let height: CGFloat = 130
    static let toolBarHeight: CGFloat = 45
    static let cancelButtonWidth: CGFloat = 70
    static let doneButtonWidth: CGFloat = 50
    static let toolBarButtonHeight: CGFloat = 35
    static let pickerMinusValue: CGFloat = 38
    static let doneButtonMinusValue: CGFloat = 65
}

public class PickerView: NSObject {
    
    // MARK: Properties
    private var selectedDate: Date?
    private var picker = UIDatePicker()
    public weak var delegate: PickerViewDelegate?
    fileprivate var alertController: UIAlertController!
    fileprivate var presentController: UIViewController!
    
    public var pickerMode: UIDatePickerMode = .date {
        didSet {
            picker.datePickerMode = pickerMode
        }
    }
    
    public var minimumDate: Date = Date() {
        didSet {
            picker.minimumDate = minimumDate
        }
    }
    
    public var maximumDate: Date = Date() {
        didSet {
            picker.maximumDate = maximumDate
        }
    }
    
    public var locale: Locale = Locale(identifier: "en_US") {
        didSet {
            picker.locale = locale
        }
    }
    
    // PickerView Initializer
    private override init() {}
    
    public convenience init(_ controller: UIViewController) {
        self.init()
        
        presentController = controller
        
        DispatchQueue.main.async {
            self.configureAlertController()
            self.configurePickerView()
            self.configureToolBar()
        }
    }
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.init(name: "Raleway - SemiBold", size: 17)
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.addTarget(self, action: #selector(self.cancelPicker(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var doneButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.init(name: "Raleway - SemiBold", size: 17)
        button.setTitle("Done", for: .normal)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.addTarget(self, action: #selector(self.donePicker(_:)), for: .touchUpInside)
        return button
    }()
}

// MARK: - Custome Methods
extension PickerView {
    
    fileprivate func configureAlertController() {
        alertController = UIAlertController(title: "", message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController.isModalInPopover = true
    }
    
    // Configure Picker View
    fileprivate func configurePickerView() {
        
        let width = presentController.view.bounds.width - 38
        let frame = CGRect(x: Origin.x, y: Origin.y, width: width, height: Size.height)
        picker.frame = frame
        picker.locale = locale
        picker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        alertController.view.addSubview(picker)
    }
    
    // Configure ToolBar
    fileprivate func configureToolBar() {
        
        // Initialize Picker tool bar
        let originY = presentController.view.bounds.origin.y + Origin.x
        let width = picker.bounds.width
        let toolFrame = CGRect(x: Origin.x, y: originY, width: width, height: Size.toolBarHeight)
        let toolView: UIView = UIView(frame: toolFrame)
        
        // Initialize Cancel tool bar button
        let buttonCancelFrame: CGRect = CGRect(x: Origin.x, y: Origin.x, width: Size.cancelButtonWidth, height: Size.toolBarButtonHeight)
        cancelButton.frame = buttonCancelFrame
        
        // Initialize Done tool bar button
        let originX = picker.bounds.width - Size.doneButtonMinusValue
        let buttonOkFrame = CGRect(x: originX, y: Origin.x, width: Size.doneButtonWidth, height: Size.toolBarButtonHeight)
        doneButton.frame = buttonOkFrame
        
        toolView.addSubview(cancelButton)
        toolView.addSubview(doneButton)
        alertController.view.addSubview(toolView)
    }
}

// MARK: - Action Methods
extension PickerView {
    
    public func show() {
        presentController?.present(alertController, animated: true, completion: nil)
    }
    
    public func dismiss() {
        presentController?.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func cancelPicker(_ sender: UIButton) {
        delegate?.didCanceled(picker)
        presentController?.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func donePicker(_ sender: UIButton) {
        delegate?.didSelected(picker, with: selectedDate ?? Date())
        presentController?.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func dateChanged(_ sender: UIDatePicker) {
        selectedDate = sender.date
        delegate?.didChanged!(sender, with: sender.date)
    }
}
