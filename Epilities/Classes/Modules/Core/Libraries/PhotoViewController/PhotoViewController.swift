//
//  PhotoViewController.swift
//  NovagemsManager
//
//  Created by Epitome Technologies on 21/12/17.
//  Copyright © 2017 Epitome Technologies. All rights reserved.
//

import UIKit

public class PhotoViewController: UIViewController {

    // MARK: - Properties
    /// IBOutlets
    @IBOutlet fileprivate weak var imageView: UIImageView?

    /// Variables
    public var photo: UIImage?

    // MARK: - UIView Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Show photo
        guard let image = photo else {return}
        showPhoto(image)
    }
}

// MARK: - Custom Methods
extension PhotoViewController {

    // Show photo
    fileprivate func showPhoto(_ image: UIImage) {
        imageView?.layer.opacity = 0.5
        UIView.animate(withDuration: 0.5, animations: {
            self.imageView?.image = image
            self.imageView?.layer.opacity = 1
        })
    }
}

// MARK: - IBAction Methods
extension PhotoViewController {

    @IBAction fileprivate func actionMethod(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
