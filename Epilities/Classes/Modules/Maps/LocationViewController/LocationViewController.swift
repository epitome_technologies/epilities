//
//  LocationViewController.swift
//  NovagemsManager
//
//  Created by Epitome Technologies on 24/12/17.
//  Copyright © 2017 Epitome Technologies. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import GoogleMaps

public protocol LocationViewControllerDelegate: NSObjectProtocol {
    func location(_ controller: UIViewController, and site: SiteLocation)
}

public class LocationViewController: UIViewController {

    // MARK: - Properties
    /// IBOutlets
    @IBOutlet fileprivate weak var searchBar: UISearchBar?
    @IBOutlet fileprivate weak var mapView: GMSMapView?
    @IBOutlet fileprivate weak var boundryButton: UIButton?

    /// Variables
    fileprivate var searchAddress = [JSON]()
    fileprivate let dropDown = DropDown()
    fileprivate var userCoordinates: CLLocationCoordinate2D!
    fileprivate var locationManager: CLLocationManager!
    fileprivate var path = GMSMutablePath()
    fileprivate var polygon = GMSPolygon()
    var siteLocation = SiteLocation.shared
    weak var delegate: LocationViewControllerDelegate?

    // MARK: - UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = Titles.addSite

        initializeDropDown()
        addRightBarButtonItem()
        initializeCLLocationManager()
        hideUnhideBoundryButton(true)
        showSiteCurrentLocationOnMap(with: siteLocation.coordinates)
    }
}

// MARK: - Custom Methods
extension LocationViewController {

    fileprivate func addRightBarButtonItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneHandler(_:)))
    }

    fileprivate func initializeCLLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    fileprivate func initializeDropDown() {
        dropDown.anchorView = searchBar
        dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = searchBar!.bounds.size.width
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.backgroundColor = UIColor.lightText
        dropDown.cellHeight = 50
        registerDropDownActionMethod()
    }

    fileprivate func hideUnhideBoundryButton(_ status: Bool) {

        if status {
            boundryButton?.isHidden = true
        } else {
            boundryButton?.isHidden = false
        }
    }

    fileprivate func registerDropDownActionMethod() {
        dropDown.selectionAction = { (index: Int, item: String) in
            self.searchBar?.resignFirstResponder()
            self.searchBar?.text = item
            self.searchPlace(of: self.searchAddress[index]["description"].stringValue)
        }
    }

    fileprivate func setSiteLocationOnMapView(_ json: JSON!) {

        let location = json[0]
        siteLocation.address = location["formatted_address"].stringValue
        siteLocation.latitude = location["geometry"]["location"]["lat"].doubleValue
        siteLocation.longitude = location["geometry"]["location"]["lng"].doubleValue
        showPlacemarkOnMapView(with: siteLocation.address, latitude: siteLocation.latitude, longitude: siteLocation.longitude)
    }

    fileprivate func showPlacemarkOnMapView(with address: String, latitude: Double, longitude: Double) {

        let address = address
        let latitude = latitude
        let longitude = longitude

        DispatchQueue.main.async {
            let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                                  longitude: longitude,
                                                  zoom: 15,
                                                  bearing: 270,
                                                  viewingAngle: 15)
            self.mapView?.camera = camera
            let position = CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude)
            let marker = GMSMarker(position: position)
            marker.title = address
            marker.map = self.mapView!
        }

        hideUnhideBoundryButton(false)
    }

    fileprivate func searchAddress(_ text: String) {

        let latitude = String(userCoordinates.latitude)
        let longitude = String(userCoordinates.longitude)
        searchLocations(with: text, latitude: latitude, longitude: longitude)
    }

    fileprivate func showSearchedAddressInDropDown(_ json: [JSON]) {

        searchAddress = json
        dropDown.dataSource.removeAll()

        for address in searchAddress {
            dropDown.dataSource.append(address["description"].stringValue)
        }

        dropDown.show()
    }

    fileprivate func showSiteCurrentLocationOnMap(with coordinates: [CLLocationCoordinate2D]) {

        // Site current location
        guard SiteLocation.shared.address != nil else {return}
        searchBar?.text = SiteLocation.shared.address
        showPlacemarkOnMapView(with: siteLocation.address, latitude: siteLocation.latitude, longitude: siteLocation.longitude)
        for coordinate in siteLocation.coordinates {
            touchHandler(coordinate)
        }
        createBoundry(with: siteLocation.coordinates)
    }

    fileprivate func createBoundry(with coordinates: [CLLocationCoordinate2D]) {

        guard coordinates.count > 2 else {return}

        for coordinate in coordinates {
            path.add(coordinate)
        }

        polygon.path = path
        polygon.strokeColor = .blue
        polygon.strokeWidth = 3
        polygon.map = mapView!
        boundryButton?.setTitle("RESET", for: .normal)
    }

    fileprivate func clearMapView(with status: Bool) {

        path.removeAllCoordinates()
        polygon.map = nil
        mapView?.clear()
        hideUnhideBoundryButton(true)
        siteLocation.coordinates.removeAll()
        boundryButton?.setTitle("CREATE BOUNDRY", for: .normal)
        guard !status else {siteLocation.address = nil; return}
        showPlacemarkOnMapView(with: siteLocation.address, latitude: siteLocation.latitude, longitude: siteLocation.longitude)
    }

    fileprivate func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(mapView!.bounds.size, false, UIScreen.main.scale)
        mapView?.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

// MARK: - Action Methods
extension LocationViewController {

    @objc fileprivate func doneHandler(_ sender: UIBarButtonItem) {
        siteLocation.image = captureScreen()
        delegate?.location(self, and: siteLocation)
        _ = navigationController?.popViewController(animated: true)
    }

    @objc fileprivate func touchHandler(_ coordinate: CLLocationCoordinate2D) {

            DispatchQueue.main.async {
                self.siteLocation.coordinates.append(coordinate)
                let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,
                                                      longitude: coordinate.longitude,
                                                      zoom: 15,
                                                      bearing: 270,
                                                      viewingAngle: 15)
                self.mapView?.camera = camera
                let position = CLLocationCoordinate2D.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
                let marker = GMSMarker(position: position)
                marker.map = self.mapView!
            }
    }
}

// MARK: - IBAction Method
extension LocationViewController {

    @IBAction fileprivate func actionMethod(_ sender: UIButton) {
        guard let title = sender.titleLabel?.text else {return}

        if title == "CREATE BOUNDRY" {
            createBoundry(with: siteLocation.coordinates)
        } else {
            clearMapView(with: false)
        }
    }
}

// MARK: - GSMapView Delegate Methods
extension LocationViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {

        guard siteLocation.address != nil else {
            showErrorAlert(title: "Map", message: "Please search site Location.")
            return
        }
        guard siteLocation.coordinates.count < 9 else {return}
        touchHandler(coordinate)
    }

    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        // Do
    }

}

// MARK: - CLLocation Manager Delegate Methods
extension LocationViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let locationArray = locations as NSArray
        guard let location = locationArray.lastObject as? CLLocation else {return}
        userCoordinates = location.coordinate
        manager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let status = isUserAuthorized(CLLocationManager.authorizationStatus())
        print(status.0, status.1)
    }

    // authorization status
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        let status = isUserAuthorized(status)
        print(status.0, status.1)
    }

    fileprivate func isUserAuthorized(_ status: CLAuthorizationStatus) -> (Bool, String) {

        switch status {
        case CLAuthorizationStatus.restricted:
            return (false, "Restricted Access to location")
        case CLAuthorizationStatus.denied:
            return (false, "User denied access to location")
        case CLAuthorizationStatus.notDetermined:
            return (false, "Status not determined")
        default:
            return (false, "Allowed to location Access")
        }
    }
}

// MARK: - Web APIs
extension LocationViewController {

    func searchLocations(with text: String, latitude: String, longitude: String) {
        let url = "\(GoogleAPI.place)autocomplete/json?input=\(text.replacingOccurrences(of: ",", with: ""))&location=\(latitude),\(longitude)&radius=5000&key=\(GoogleAPI.key)"
        guard let requsestUrl = URL.init(string: url) else {return}
        var request = URLRequest(url: requsestUrl)
        request.httpMethod = "GET"

        Api.default.process(with: request, complition: { (response, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print(error.debugDescription)
                    return
                }
                guard let jsonData = response else {return}
                self.showSearchedAddressInDropDown(jsonData["predictions"].arrayValue)
            }
        })
    }

    func searchPlace(of location: String) {

        var filteredLocation = location.replacingOccurrences(of: ",", with: "")
        filteredLocation = filteredLocation.replacingOccurrences(of: " ", with: "+")
        let url = "\(GoogleAPI.place)textsearch/json?query=\(filteredLocation)&key=\(GoogleAPI.key)"
        guard let requsestUrl = URL.init(string: url) else {return}
        var request = URLRequest(url: requsestUrl)
        request.httpMethod = "GET"

        Api.default.process(with: request, complition: { (response, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print(error.debugDescription)
                    return
                }
                guard let jsonData = response else {return}
                self.setSiteLocationOnMapView(jsonData["results"])
            }
        })
    }
}

// MARK: - UISearchBar Delegate Methods
extension LocationViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            clearMapView(with: true)
        }
        searchAddress(searchText)
    }
}
