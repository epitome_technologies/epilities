# Epilities

[![CI Status](http://img.shields.io/travis/Arsh Aulakh/Epilities.svg?style=flat)](https://travis-ci.org/Arsh Aulakh/Epilities)
[![Version](https://img.shields.io/cocoapods/v/Epilities.svg?style=flat)](http://cocoapods.org/pods/Epilities)
[![License](https://img.shields.io/cocoapods/l/Epilities.svg?style=flat)](http://cocoapods.org/pods/Epilities)
[![Platform](https://img.shields.io/cocoapods/p/Epilities.svg?style=flat)](http://cocoapods.org/pods/Epilities)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Epilities is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
source 'https://bitbucket.org/epitome_technologies/podspecs.git'
source 'https://github.com/CocoaPods/Specs.git'

pod 'Epilities'
pod 'GoogleMaps'
pod 'GooglePlaces'
```

## Author

Arsh Aulakh, arsh@epitometechnologies.com

## License

Epilities is available under the MIT license. See the LICENSE file for more info.
